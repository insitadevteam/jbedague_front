<h2>INSITACTION</h2>
<h3>Test suite entretien pour un stage en
développement frontend</h3>

<h4>Consignes</h4>
<ul>
<li>Respect des standards W3C</li>
<li>Utilisation d’un Framework CSS/JS</li>
<li>Page Responsive</li>

<li>
<ul>Page interactive
<li>Animation CSS</li>
<li>Rollover</li>
<li>Effets d'apparitions</li>
</ul>
<li>Menu Off-Canvas</li>
</ul>

<h4>Veuillez préciser:</h4>
<ul><li>Le temps passé </li>
<li>Le(s) outil(s) utilisé(s) </li>
<li>Les difficultés rencontrées </li></ul>



<h4>Bon courage, l’équipe INSITACTION.</h4>
